package br.com.projeto.cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) throws UnknownHostException, IOException {
	
		System.out.println("Iniciando cliente.");
		
		System.out.println("Iniciando conex�o com o servidor.");
		
		Socket socket = new Socket("127.0.0.1", 2525);
		
		System.out.println("Conex�o estabelecida.");
		
		InputStream input = socket.getInputStream();
		OutputStream output = socket.getOutputStream();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(input));
		PrintStream out = new PrintStream(output);
		
		Scanner scanner = new Scanner(System.in);
		
		while(true) {
			System.out.print("Digite uma mensagem: ");
			String mensagem = scanner.nextLine();
			
			out.println(mensagem);
			
			if("FIM".equals(mensagem)) {
				break;
			}
			
			mensagem = in.readLine();
			
			System.out.println(
					"Mensagem recebida do servidor: " +
					 mensagem
					);
		}
		
		System.out.println("Encerrando conex�o.");
		
		scanner.close();
		in.close();
		out.close();
		socket.close();

	}

}
